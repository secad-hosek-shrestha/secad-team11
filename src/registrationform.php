
<!DOCTYPE html>
<html lang="en">
<center>
<head>
  <link rel="stylesheet" type="text/css" href="./style.css">
  <meta charset="utf-8">
  <title>Registration page - SecAD</title>
  <div id = 'logo'>
            <img src="./images/logos/Mercury2.png" alt="Mercury2">
  </div>
</head>
<body>
  <link rel="stylesheet" type="text/css" href="./style.css">
      	<h1 style = "font-family:helvetica">Team Project, SecAD</h1>
        <h2 style = "font-family:helvetica">Team 11: Beth Hosek and Shara Shrestha</h2>

<?php
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <form action="addnewuser.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" pattern="^[\w]+$" title="Please enter a valid username" 
                placeholder="Your username" 
                onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');"/> <br>
          
                Password:<input type="password" class="text_field" name="password" required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$" 
                placeholder="Your password"
                title="Password must have at least 8 characters with 1 special symbol !@#$%^& 1 number, 1 lowercase, and 1 UPPERCASE" 
                placeholder="Your password" 
                onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;"/> <br>
                Retype Password: <input type="password" class="text_field" name="repassword"
                  placeholder="Retype your password" required
                  title="Password does not match"
                  onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br>

                Email:<input type="text" class="text_field" name="email" required pattern="([a-z0-9][-a-z0-9_\+\.]*[a-z0-9])@([a-z0-9][-a-z0-9\.]*[a-z0-9]\.(edu|com|org)|([0-9]{1,3}\.{3}[0-9]{1,3}))"
                title="Please enter a valid email" 
                placeholder="Your email address" 
                onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');"/> <br>
                
                Phone:<input type="text" class="text_field" name="phone"
                required pattern="^[0-9]{10}$"
                title="Please enter a valid 10 digit phone number without any characters, just numbers" 
                placeholder="Your phone number" /> <br>

                <button class="button" type="Register">
                  Sign Up
                </button>
          </form>

</body>
<center>
</html>

