<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" type="text/css" href="style.css">
  <meta charset="utf-8">
  <title>Mercury Login Page</title>
</head>
<center>
<body>
        <div class="text-center" id = 'logo'>
            <img src="./images/logos/Mercury1.png" alt="Mercury1">
        </div>

        <h1 style = "font-family:helvetica">SecAD Team 11</h1>
        <h2 style = "font-family:helvetica">Beth Hosek and Shara Shrestha</h2>

<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
  //some code here
  echo "Current time: " . date("Y-m-d h:i:sa")
?>
          <br>
          <form action="index.php" method="POST" class="form login">
                Username:<input type="text" class="text_field" name="username" /> <br>
                Password: <input type="password" class="text_field" name="password" /> <br>
                <button class="button" type="submit">
                  Login
                </button>
          </form>
          <form action="registrationform.php" method="POST" class="form new user">
                <button class="button" type="submit">
                  Sign Up
                </button>
          </form>

</body>
</center>
</html>

