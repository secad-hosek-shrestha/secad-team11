<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	$mysqli = new mysqli('localhost', 'admin', 'hermes', 'SecadTeam11');
	if ($mysqli->connect_errno){
		printf("Database connection failed : %s\n", $mysqli->connect_error);
		exit();
	}

	function securechecklogin($username, $password) {
		global $mysqli;
		$prepared_sql = "SELECT * FROM users WHERE username=? AND password = password(?)";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$stmt->bind_param("ss",$username,$password);
		if(!$stmt->execute()) echo "Execute Error";
		if(!$stmt->store_result()) echo "Store result error";
		$result = $stmt;
		if($result->num_rows ==1)
			return TRUE;
		return FALSE;
  	}
  	
	function changepassword($username, $newpassword){
		global $mysqli;
		$prepared_sql = "UPDATE users SET password=password(?) WHERE username=?;";
		echo "DEBUG>prepared_sql= $prepared_sql\n";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "<script>alert('prepare error');</script>";
			return FALSE;
		}
		$stmt->bind_param("ss", $newpassword, $username);
		if(!$stmt->execute()){
			echo "<script>alert('execute error');</script>";
			return FALSE;
		} 
		return TRUE;
	}
	function sanitize_input($input){
		$input = trim($input);
		$input = stripslashes($input);
		$input = htmlspecialchars($input);
		return $input;
	}
	function validateUsername($username){
		$username = sanitize_input($username);
		if (empty($username) or strlen($username) < 3){
			return FALSE;
		}
		return TRUE;
	}
	function validatePassword($password){
		$password = sanitize_input($password);
		if (empty($password) or
			!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$/", $password)){
			return FALSE;
		}
		return TRUE;
	}
	function addnewuser($username, $password, $email, $phone){
		global $mysqli;
		$prepared_sql = "INSERT INTO users (username, password, email, phone) VALUES (?, password(?), ?, ?);";
		echo "DEBUG:database.php ->addnewuser->prepared_sql= $prepared_sql\n";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "prepare error!\n";
			echo "$mysqli->error\n";
			return FALSE;
		}
		$stmt->bind_param("ssss", $username, $password, $email, $phone);
		if (!$stmt->execute()){
			echo "execute error!\n";
			echo "$mysqli->error\n";
			return FALSE;
		}
		return TRUE;
	}
	//function to display posts on the page
	function getPosts($mysqli){
		global $mysqli;
		$sql = "SELECT * FROM posts";
		$result = $mysqli->query($sql);

		/*
		 * displaying the posts
		 * and giving the reply functionality to posts that arent from a specific user
		 */
		while ($row = $result->fetch_assoc()){
			$username = $row['username'];
			$sql2 = "SELECT * FROM users WHERE username='$username'";
			$result2 = $mysqli->query($sql2);
			if ($row2 = $result2->fetch_assoc()){
				echo "<div class='comment-box'><p>";
				echo $row2['username']."<br>";
				echo $row['date']."<br><br>";
				echo nl2br($row['message']);
				echo "</p>";

				if ($_SESSION['username'] == $row2['username'] or $row2['enabled'] == 1){
				echo "<form class='delete-form' method='POST' action='".deleteComments($mysqli)."'>
					<input type='hidden' name='PostId' value='".$row['PostId']."'>
					<button type='submit' name='postDelete'>Delete</button>
				</form>
				<form class='edit-form' method='POST' action='editPost.php'>
					<input type='hidden' name='PostId' value='".$row['PostId']."'>
					<input type='hidden' name='username' value='".$row['username']."'>
					<input type='hidden' name='date' value='".$row['date']."'>
					<input type='hidden' name='message' value='".$row['message']."'>
					<button>Edit</button>
				</form>";

				}
			}
		}
	}//end getPosts()


	function addPost($newtitle, $newcontent, $newdate, $username){

		global $mysqli;

		/* values in the posts table
		PostId int(20) NOT NULL AUTO_INCREMENT,
  		owner varchar(50) NOT NULL,
  		date datetime NOT NULL,
  		content text DEFAULT NULL,
  		title varchar(50) DEFAULT NULL*/

  		$prepared_sql = "INSERT INTO posts (owner, date, content, title) VALUES (?, ?, ?, ?);";
		//create sql statement
		echo "DEBUG:database.php ->addpost->prepared_sql= $prepared_sql\n";
		if (!$stmt = $mysqli->prepare($prepared_sql)){
			echo "prepare error!\n";
			echo "$mysqli->error\n";
			return FALSE;
		}
		$stmt->bind_param("ssss", $username, $newdate, $newcontent, $newtitle);
		if (!$stmt->execute()){
			echo "execute error!\n";
			echo "$mysqli->error\n";
			return FALSE;
		}
		return TRUE;
		//$sql = "UPDATE posts SET message='$message' WHERE PostId='$PostId'";

		$result = $mysqli->query($sql);
		header("Location: index.php");
		
	}//end addPosts()

	function editPost($newtitle, $newcontent, $newdate, $postid){
		global $mysqli;

/*		if(isset($_POST['postSubmit'])){
		$PostId = $_POST['PostId'];
		$username = $_POST['username'];
		$date = $_POST['date'];
		$message = $_POST['message'];
*/

		//create sql statement
		$sql = "UPDATE posts SET message='$message' WHERE PostId='$PostId'";

		$result = $mysqli->query($sql);
		header("Location: index.php");

	}//end editPosts()

	function deletePost($mysqli){
		global $mysqli;
		if(isset($_POST['postDelete'])){
		$PostId = $_POST['PostId'];


		//create sql statement
		$sql = "DELETE FROM posts WHERE PostId='$PostId'";

		$result = $mysqli->query($sql);
		header("Location: index.php");
		}
	}//end deletePost()

	function setPost($mysqli){
		global $mysqli;
		if(isset($_POST['postSubmit'])){
		$username = $_POST['username'];
		$date = $_POST['date'];
		$message = $_POST['message'];


		//create sql statement
			$sql = "INSERT INTO posts (username, date, message) VALUES ('$username', '$date', '$message')";

		$result = $mysqli->query($sql);
		}
	}//end setPost()

	function seeUsers($username){
		
		global $mysqli;
		$conn = new mysqli('localhost', 'admin', 'hermes', 'SecadTeam11');
		$prepared_sql = "SELECT * FROM users WHERE username=? AND super = ?";
		if(!$stmt = $mysqli->prepare($prepared_sql))
			echo "Prepared Statement Error";
		$sup = 1;
		$stmt->bind_param("ss",$username,$sup);
		if(!$stmt->execute()) echo "Execute Error in checking superuser";
		if(!$stmt->store_result()) echo "Store result error in checking superuser";
		$result = $stmt;
		if($result->num_rows ==1){

			$sql = "SELECT * FROM users";
			$result = $conn->query($sql);
			if ($result->num_rows > 0){
			//output the data
				echo"<h2>Superusers can view all registered users </h2>";
				while ($row = $result->fetch_assoc()){
					echo "<div><p>";
					echo "Username: ";
					echo $row["username"];
					echo "     Password: Hidden     Email: ";
					echo $row["email"];
					echo "     Phone Number: ";
					echo $row["phone"];
					echo "     Super User: ";
					echo $row["super"];
					echo"</p></div><br>";
				}
			}
			$conn->close();
			return TRUE;
		}
		$conn->close();
		return FALSE;


	}

?>
