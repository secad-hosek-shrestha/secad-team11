<?php
	
	require 'database.php';
    //require "session_auth.php";
    //DEBUG>echo "<script>alert('You got this far - 1');</script>";
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	$lifetime = 15 * 60;
	$path = "/teamproject";
	$domain= "localhost";
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start(); 

	$username;
	$password;
	
	//DEBUG>echo "<script>alert('You got this far - 2');</script>";
	if (isset($_POST["username"]) and isset($_POST["password"])){
		$username = $_POST["username"];
		$password = $_POST["password"];
		if (securechecklogin($username,$password)) {
			$_SESSION["logged"] = TRUE;
			$_SESSION["username"] = $_POST["username"];
			$_SESSION["password"] = $_POST["password"];
			$_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
		}else{
			echo "<script>alert('Invalid username/password');</script>";
			unset($_SESSION["logged"]);
			header("Refresh:0; url=form.php");
			die();

		}
	}	
	//directly from phu's powerpoint
	if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
		echo "<script>alert('You have not logged in yet. Please login first!');</script>";
		header("Refresh:0; url=form.php");
		die();
	}
	//DEBUG>echo "<script>alert('You got this far - 4');</script>";
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		echo "<script>alert('Session hijacking detected, please login again');</script>";
		header("Refresh:0; url=form.php");
		die();
	}

	/*if($username == NULL or $password == NULL)
	{
		$username = $_SESSION["username"];
		$password = $_SESSION["password"];
	}*/

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="./style.css">
	<title> Mercury Homepage </title>
	
</head>
<script>
function startTime() {
    document.getElementById('clock').innerHTML = new Date();
    setTimeout(startTime, 500);
}
var sanitizeHTML = function (str) {
  var temp = document.createElement('div');
  temp.textContent = str;
  return temp.innerHTML;
};
</script>
<body>
<center>
	<a href="changepasswordform.php">
		<button class='homepage-button' type='submit' action='changepasswordform.php'>Change Password</button></a>
	<a href="logout.php">
		<button class='homepage-button' type='submit' action='logout.php'>Log Out</button></a>
	<div id = 'logo'>
            <img src="./images/logos/Mercury2.png" alt="Mercury2">
    </div>
	<h1 style = "font-family:helvetica">
		Welcome, <?php echo htmlentities($_SESSION["username"]); ?>!
	</h1>

	<h2 style = "font-family:helvetica">Write a Post</h2>
	<!--- text area under here to write post, clicking the Post button should submit it via getPosts() function-->

	<form method="POST" action='addpost.php'>
		<textarea class="title-bar" placeholder="Give your post a title" name="title"></textarea><br>
		<textarea placeholder="What's on your mind?" name="content"></textarea><br><br>
		<button class='homepage-button' type='submit' name='Post'> Write Post</button>
	</form>

	<br><br><br>
	

<?php
		$username = $_SESSION["username"];
		$password = $_SESSION["password"];
		$dbserver = "localhost";
		$dbusername = "admin";
		$dbpassword = "hermes";
		$dbname = "SecadTeam11";
		$date = date("Y-m-d H:i:s");

		//connect to db for post echoing
		$conn = new mysqli($dbserver, $dbusername, $dbpassword, $dbname);

		/*DEBUG>$insertsql = "INSERT INTO posts (owner, date, content, title) VALUES ('$username', '$date', 'some new content for testing', 'a new title')";
			$conn->query($insertsql);*/

		//query the database
		$sql = "SELECT * FROM posts";
		$result = $conn->query($sql);
		if ($result->num_rows > 0){
			//output the data
			while ($row = $result->fetch_assoc()){
				if ($row["owner"] == $_SESSION["username"]){ //if the post is from the same user that's
					echo "<div class='comment-box'><p><b>";									//logged in or if the user is a superuser
					echo $row["owner"]."</b><br>";
					echo $row["title"];
					echo " | ";
					echo $row["date"]."<br>";
					echo $row["content"];
					echo "</p>";

					//delete post button
					echo "<form class='delete-form' method='POST' action='".deletePost($conn)."'>";
						echo "<input type='hidden' name='PostId' value='".$row['PostId']."'>";
						echo "<button type='submit' name='postDelete'>Delete</button>";
					echo "</form>";

					//edit post button
					echo "<form class='edit-form' method='POST' action='editpost.php'>";
						echo "<input='hidden' name='PostId' value='".$row['PostId']."'";
						echo "<input='hidden' name='owner' value='".$row['owner']."'>";
						echo "<input='hidden' name='date' value='".$row['date']."'>";
						echo "<input='hidden' name='content' value='".$row['content']."'>";
						echo "<button>Edit</button>";
					echo "</form>";

					//end of content in comment box
					echo "</div><br>";
				} else {
					echo "<div class='comment-box'><p><b>";
				echo $row["owner"]."</b><br>";
				echo $row["title"];
				echo " | ";
				echo $row["date"]."<br>";
				echo $row["content"];
				echo "</p>";

				//else you can reply to a post because it is not yours
				echo "<form class='comment-form' method='POST' action='addcomment.php'>";
						echo "<input='hidden' name='PostId' value='".$row['PostId']."'";
						echo "<input='hidden' name='username' value='".$row['owner']."'>";
						echo "<input='hidden' name='date' value='".$row['date']."'>";
						echo "<input='hidden' name='content' value='".$row['content']."'>";
						echo "<button>Comment</button>";
					echo "</form>";
				echo "</div><br>";
				}
			}
		} else {
			echo "0 posts found";
		}
		$conn->close();

		seeUsers($_SESSION["username"]);

?>

</center>
</body>
</html>