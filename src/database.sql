
DROP TABLE IF EXISTS `comments`;
DROP TABLE IF EXISTS `posts`;
DROP TABLE IF EXISTS `users`;


CREATE TABLE users (
  username varchar(50) PRIMARY KEY,
  password varchar(100) NOT NULL,
  email varchar(100) NOT NULL,
  phone varchar(100) NOT NULL,
  super tinyint(1) DEFAULT '0'
);


LOCK TABLES `users` WRITE;

INSERT INTO `users` VALUES ('admin', password('hermes'), 'admin@secad.org', '1234567890',1),('beth', password('hosek'), 'hoseke1@udayton.edu','6302453869',1),('shara',password('shrestha'),'shresthas6@udayton.edu','3014379407',1);

UNLOCK TABLES;

CREATE TABLE posts (
  PostId int(20) NOT NULL AUTO_INCREMENT,
  owner varchar(50) NOT NULL,
  date datetime NOT NULL,
  content text DEFAULT NULL,
  title varchar(50) DEFAULT NULL,
  PRIMARY KEY (PostId),
  FOREIGN KEY (owner) REFERENCES `users`(`username`) ON DELETE CASCADE
);







CREATE TABLE comments (
    id int(11) AUTO_INCREMENT,
    PostId int(20) NOT NULL,
    content text NOT NULL,
    date timestamp DEFAULT CURRENT_TIMESTAMP, 
    owner varchar(50) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (owner) REFERENCES `users`(`username`) ON DELETE CASCADE,
    FOREIGN KEY (postid) REFERENCES `posts`(`PostId`) ON DELETE CASCADE
);
