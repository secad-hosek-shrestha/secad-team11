<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	date_default_timezone_set('America/New_York');
	require "database.php";

?>


<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="utf-8">
	<title>Mercury Homepage</title>
	<form method="POST" action='editpost.php'>
		<textarea class="title-bar" placeholder="Change post title" name="title"></textarea><br>
		<textarea placeholder="What's on your mind?" name="content"></textarea><br><br>
		<button class='homepage-button' type='submit' name='Post'> Write Post</button>
	</form>
</head>
<body>