<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	require "session_auth.php";
	require "database.php";
	$newtitle = sanitize_input($_REQUEST["title"]);
	$newcontent = sanitize_input($_REQUEST["content"]);
	
	//$postid = sanitize_input($_REQUEST["postid"];
	$newdate = date("Y-m-d H:i:s");
	$username = $_SESSION["username"];
	$nocsrftoken = $_SESSION["nocsrftoken"];
	if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
		echo "<script>alert('Cross-site request forgery is detected!');</script>";
		header("Refresh:0; url=logout.php");
		die();
	}
	if ($username!=$_SESSION["username"]) {
		echo "<script>alert('Cannot edit others' posts!');</script>";
		header("Refresh:0; url=logout.php");
		die();
	}
	//DEBUG>echo "<script>alert('right before you go to addpost in database.php');</script>";
	addPost($newtitle, $newcontent, $newdate, $username)
?>
<a href="index.php">Mercury Home</a> | <a href="logout.php">Logout</a>