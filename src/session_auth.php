<?php
	$lifetime = 15 * 60; //15 min
	$path = "/teamproject";
	$domain= "localhost"; //personal IP address
	$secure = TRUE;
	$httponly = TRUE;
	session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
	session_start();

	if ($_SESSION["logged"] != TRUE){
		//!isset($_SESSION["logged"]) or
		echo "<script>alert('You have not logged in yet. Please login first!');</script>";
		session_destroy();
		header("Refresh:0; url=form.php");
		die();
	}
	//DEBUG>echo "<script>alert('You got this far - 4');</script>";
	if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
		echo "<script>alert('Session hijacking detected, please login again');</script>";
		header("Refresh:0; url=form.php");
		die();
	}
	?>
