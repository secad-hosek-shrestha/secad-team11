# Team Project: Secure Application Development #

University of Dayton

Department of Computer Science

CPS 475/575, Spring 2021

Instructor: Dr. Phu Phung



# Team 11 members

*	Beth Hosek - hoseke1@udayton.edu
*	Shara Shrestha - shresthas6@udayton.edu

![logo](./src/images/logos/Mercury2.png)

# Final Report  



# 1. Introduction

_Overview of your project design, development, and your achievement. Remember to **push all your code to your private repository** and provide the link in this section._

Link to commits: [https://bitbucket.org/secad-hosek-shrestha/secad-team11/commits/]

The goal of this project is to make a mini-Facebook application, which we are calling Mercury. 


![logo](./src/images/logos/Mercury3.png)


We have implemented this in _3 different_ sprints:

* In the 1st sprint, we created a form for user registration which handles the users' username and password. These usernames and passwords, once validated, get inserted into the user database.
* In the 2nd sprint, we created tables for posts, users, and comments. We also allowed for the users that are logged in to be able to add new posts, view and add comments on posts, and update or delete their posts. 
* In the 3rd sprint, we created hard coded superusers that can login and view other registered users. These administrative users are granted access to features that other users are not given.


# 2. Design

_Describe your design of:_ 

*   Database - our database currently has three tables. The Users table stores usernames, passwords, emails, and phone numbers; the Posts table stores postid, owner (FK, references username from users), date, content, and title; and the Comments table stores id, postid, content, date, and owner (FK, username from users).
*   User Interface - the user interface is clean and simple, while integrating colors from our logo throughout the application. In order to maintain cohesiveness, different variations of the logo are placed on each page, and every page is centered in order to take full advantage of the page space. The font pairing and color scheme are both modern, while the basic functionality of the interface is quite traditional.  
*   Functionality - users can register for an account, log in, create posts, edit or delete posts, and comment/edit comments on posts.

# 3. Implementation & Security Analysis

_Include a brief explanation of your implementation and the security aspects based on the following questions:_

*   How did you apply the security programming principles in your project?
    * We applied input validation, access control, and output sanitization by paying attention to the user interacting with the application and sending information to the database. 
*   Have you used defense-in-depth and defense-in-breath principles in your project?
    * By combining different security principles with each other, we were able to implement a defense-in-depth approach.  
*   What database security principles have you used in your project?
    * One way we used the principle of least privilege in our project was by implementing super-users in order to have administrative users that have access to certain features other, regular users, do not.
*   Is your code robust and defensive? How?
    *   Yes, our code is robust because we were able to look over and debug the functionalities of our application in order to allow it to execute what it is supposed to do.
*   How did you defend your code against known attacks such as including XSS, SQL Injection, CSRF, Session Hijacking
    *   Our code does not validate the inputs on our application so that scripts cannot run, this defends against attacks such XSS. We have implemented preppared sql statements to ensure that we are retrieving necessary query in order to defend against SQLi attacks. And our code also uses a session token and sets a lifetime for each users session to defend against CSRF.  
*   How do you separate the roles of super users and regular users?
    *   In order to separate the roles of super users and regular users, we added access control to our program security. We implemented super-users to have control over the regular users, separating them by design in the users table by assigning them an attribute that is "enabled." We decided that users can only enable their status internally in order to obtain it. This eliminates the attack surface and makes it difficult for someone to register themselves as an super-users through the registration page.


# 4. Demo (Screenshots)

_You need to capture screenshots to demonstrate how your web application works. The screenshots must be accompanied by a short description of its functionalities following the implementation as below:_

* ![demo1](./src/images/demos/demo1.png)
    * Everyone can register a new account 
* ![demo2](./src/images/demos/demo2.png)
    * Registered users can login
* ![demo3](./src/images/demos/demo3.png)
    * Logged in users can:
        * ![demo13](./src/images/demos/demo13.png)
        * Add a new post
    * ![demo7](./src/images/demos/demo7.png)
        *   See previous posts
        * Edit their own post
        * Add comments on any post
    * ![demo10](./src/images/demos/demo10.png)
        *  Change password
            * ![demo11](./src/images/demos/demo11.png)
* ![demo4](./src/images/demos/demo4.png)
    * Users can logout and get redirected to the login page
* ![demo5](./src/images/demos/demo5.png)
    * Superuser can view all registered users
    * ![demo6](./src/images/demos/demo6.png)
        * Whereas regular logged-in users cannot access the link for superusers 
    * Superuser can disable an account
        *   The disabled account cannot log in 
        *   Superuser can enable the disabled account
        *   The enabled user can log in	
* ![demo8](./src/images/demos/demo8.png)
* ![demo9](./src/images/demos/demo9.png)
    *   A regular logged-in user can delete her own existing posts but cannot delete the posts of others
* ![demo12](./src/images/demos/demo12.png)
    *   CSRF attack to delete a post should be detected and prevented
    *   A logged-in user can have a real-time chat with other logged-in users

# Appendix

*   secad-team11
    *   src
        *   images
            *   demos
                *   ![demos](./src/images/demos/demo1.png)

                *   ![demos](./src/images/demos/demo2.png)

                *   ![demos](./src/images/demos/demo3.png)

                *   ![demos](./src/images/demos/demo4.png)

                *   ![demos](./src/images/demos/demo5.png)

                *   ![demos](./src/images/demos/demo6.png)

                *   ![demos](./src/images/demos/demo7.png)

                *   ![demos](./src/images/demos/demo8.png)

                *   ![demos](./src/images/demos/demo9.png)

                *   ![demos](./src/images/demos/demo10.png)

                *   ![demos](./src/images/demos/demo11.png)

                *   ![demos](./src/images/demos/demo12.png)

            *   logos
                *   ![logos](./src/images/logos/Mercury1.png)

                *   ![logos](./src/images/logos/Mercury2.png)

                *   ![logos](./src/images/logos/Mercury3.png)
        *   sslkey
            *   changepasswordform.php
            *   database.php
            *   registrationform.php
            *   secad.crt
            *   secad.key
        *   addcomment.php
            ```php 
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

                require "session_auth.php";
                require "database.php";
                $nocsrftoken = sanitize_input($POST["nocsrftoken"]);
                $newcontent = sanitize_input($_REQUEST["content"]);
                $postid = sanitize_input($_REQUEST["postid"];
                $newdate = sanitize_input($_REQUEST["date"];
                $username = sanitize_input($_REQUEST["username"];
                $id = sanitize_input($_REQUEST["id"];

                if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
                    echo "<script>alert('Cross-site request forgery is dete
                    cted!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
                if ($username!=$_SESSION["username"]) {
                    echo "<script>alert('Cannot edit others' posts!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
                editpost($newcontent, $newdate, $postid);
            ?>
            ```
            ```html 
            <a href="index.php">Home</a> | <a href="logout.php">Logout</a> 
            ```
        *   addnewuser.php
            ```php
            <?php
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);
                error_reporting(E_ALL);
                require "database.php";
                $username = sanitize_input($_POST["username"]);
                $password = sanitize_input($_POST["password"]);
                $email = sanitize_input($_POST["email"]);
                $phone = sanitize_input($_POST["phone"]);
                if (!validateUsername($username) or !validatePassword($password)){
                    echo "<script>alert('Please enter valid username/password!');</script>";
                    header("Refresh:0; url=registrationform.php");
                    die();
                }
                echo "DEBUG: addnewuser.php> username= $username; password= $password email = $email phone = $phone";
                if (addnewuser($username, $password, $email, $phone)){
                    echo "DEBUG: addnewuser.php> $username is added";
                    echo "<script>alert('Your new account is successfully registered. Please login!);</script>";
                    header("Refresh:0; url=form.php");
                } else{
                    echo "DEBUG:addnewuser.php> $username cannot be added";
                    echo "<script> alert('Something was wrong with your registration. Please try again!');</script>";
                    header("Refresh:100; url=registrationform.php");
                }
            ?>
            ```
        * addpost.php
            ```php 
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

                require "session_auth.php";
                require "database.php";
                $nocsrftoken = sanitize_input($_POST["nocsrftoken"]);
                $newcontent = sanitize_input($_REQUEST["content"]);
                $newtitle = sanitize_input($_REQUEST["title"];
                //$postid = sanitize_input($_REQUEST["postid"];
                $newdate = sanitize_input($_REQUEST["date"];
                $username = sanitize_input($_REQUEST["username"];

                if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
                    echo "<script>alert('Cross-site request forgery is detected!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
                if ($username!=$_SESSION["username"]) {
                    echo "<script>alert('Cannot edit others' posts!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
                echo "<script>alert('right before you go to editpost in database.php');</script>";
                addPost($newtitle, $newcontent, $newdate);
            ?>
            ```
            ```html
            <a href="index.php">Home</a> | <a href="logout.php">Logout</a>
            ```
        * changepassword.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

                require "session_auth.php";
                require "database.php";
                $username= $_SESSION["username"];
                $newpassword = $_REQUEST["newpassword"];
                $nocsrftoken = $POST["nocsrftoken"];
                if (isset($username) AND isset($newpassword)){
                    echo "DEBUG:changepassword.php->Got: username=$username;newpassword=$newpassword\n<br>";
                    if( changepassword($username, $newpassword)){
                        echo "<h4>The new password has been set.</h4>";
                    }else{
                        echo "<h4>Error: Cannot change the password.</h4>";
                    }
                }else{
                    echo "no provided username/password to change";
                    exit();
                }
                if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
                    echo "<script>alert('Cross-site request forgery is detected!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
            ?>
            ```
            <a href="index.php">Home</a> | <a href="logout.php">Logout</a>
            ```
        * changepasswordform.php
            ```html
            <!DOCTYPE html>
            <link rel="stylesheet" type="text/css" href="style.css">
            ```
            ```php
            <?php
                require "session_auth.php";
                $rand= bin2hex(openssl_random_pseudo_bytes(16));
                $_SESSION["nocsrftoken"] = $rand;
            ?>
            <?php
            require "session_auth.php"
            ?>
            ```
            ```html
            <center>
            <html lang="en">
            <head>
            <meta charset="utf-8">
            <title>Login page - SecAD</title>
            </head>
            <body>
                    <h1>Change Password, SecAD</h1>
                    <h2> By Beth Hosek and Shara Shrestha </h2>
            ```
            ```php
            <?php
            //some code here
            echo "Current time: " . date("Y-m-d h:i:sa")
            ?>
            ```
            ```html
                        <form action="changepassword.php" method="POST" class="form login">
                            New Password: <input type="password" class="text_field" name="newpassword" /> <br>
                            <button class="button" type="submit">
                            Change password
                            </button>
                        </form>

            </body>
            </center>
            </html>
            ```
        * changepost.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

                require "session_auth.php";
                require 'database.php';
                $nocsrftoken = sanitize_input($POST["nocsrftoken"]);
                $newcontent = sanitize_input($_REQUEST["newcontent"]);
                $newtitle = sanitize_input($_REQUEST["newtitle"];
                $postid = sanitize_input($_REQUEST["postid"];
                $newdate = sanitize_input($_REQUEST["newdate"];
                $username = sanitize_input($_REQUEST["username"];
                if (isset($postid)){
                    echo "DEBUG:changepost.php->Got: postid=$postid;\n<br>";
                } else {
                    echo "postid doesn't exist";
                    exit();
                }
                if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
                    echo "<script>alert('Cross-site request forgery is detected!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
                if ($username!=$_SESSION["username"]) {
                    echo "<script>alert('Cannot edit others' posts!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                }
                editpost($newtitle, $newcontent, $newdate, $postid);
            ?>
            ```
            ```html
            <a href="index.php">Home</a> | <a href="logout.php">Logout</a>
            ```
        * changepostform.php
            ``` php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
                #require "session_auth.php";
                $rand= bin2hex(openssl_random_pseudo_bytes(16));
                $_SESSION["nocsrftoken"] = $rand;
            ?>
            ```
            ```html
            <!DOCTYPE html>
            <html lang="en">
            <head>
            <meta charset="utf-8">
            <title>Login page - SecAD</title>
            </head>
            <body>
                    <h1>Change Post, Mercury</h1>
                    <h2>Team 11: Beth Hosek and Shara Shrestha</h2>
            ```
            ```php
            <?php
            $postid = $_REQUEST["postid"];
            $username = $_REQUEST["username"];
            echo "Current time: " . date("Y-m-d h:i:sa");

            $prepared_sql = "SELECT title, content, date FROM posts WHERE postid=$postid";

                //create prepared statement from the string
                if(!$stmt = $mysqli->prepare($prepared_sql))
                    echo "Prepared SQL Statement Error";

                //execute the statement
                if(!$stmt->execute())
                    echo "Execute error";
                //Bind the results to variables
                $title = NULL; $content = NULL; $date=NULL;
                if(!$stmt->bind_result($title, $content, $date))
                    echo "Binding failed ";

            if ($stmt->fetch()){
            echo htmlentities($title) . ", " . htmlentities($content) . ", " .
                htmlentities($date) . "<br>";
            }
            ?>
            ```
            ```html
                        <form action="changepost.php" method="POST" class="form login">
                            <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
                            <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                            <input type="hidden" name="newdate" value="<?php echo date('Y-m-d h:i:sa'); ?>" />
                            <input type="hidden" name="username" value="<?php echo $username; ?>" />
                            Edit post title: <input class="text_field" name="newtitle" value="<?php echo $title; ?>" /> <br>
                            Edit post content: <input class="text_field" name="newcontent" value="<?php echo $content; ?>" /> <br>
                            <button class="button" type="submit">
                            Edit post
                            </button>
                    </form>
                    <form action="deletepost.php" method="POST" class="form login">
                            <input type="hidden" name="username" value="<?php echo $username; ?>" />
                            <input type="hidden" name="postid" value="<?php echo $postid; ?>" />
                            <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                            <button class="button" type="submit">
                            Delete post
                            </button>
                    </form>
            </body>
            </html>
            ```
        * database.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
                $mysqli = new mysqli('localhost', 'admin', 'hermes', 'SecadTeam11');
                if ($mysqli->connect_errno){
                    printf("Database connection failed : %s\n", $mysqli->connect_error);
                    exit();
                }

                function securechecklogin($username, $password) {
                    global $mysqli;
                    $prepared_sql = "SELECT * FROM users WHERE username=? AND password = password(?)";
                    if(!$stmt = $mysqli->prepare($prepared_sql))
                        echo "Prepared Statement Error";
                    $stmt->bind_param("ss",$username,$password);
                    if(!$stmt->execute()) echo "Execute Error";
                    if(!$stmt->store_result()) echo "Store result error";
                    $result = $stmt;
                    if($result->num_rows ==1)
                        return TRUE;
                    return FALSE;
                }
                
                function changepassword($username, $password){
                    global $mysqli;
                    $prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
                    echo "DEBUG>prepared_sql= $prepared_sql\n";
                    if (!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
                    $stmt->bind_param("ss", $newpassword, $username);
                    if(!$stmt->execute()) return FALSE;
                    return TRUE;
                }
                function sanitize_input($input){
                    $input = trim($input);
                    $input = stripslashes($input);
                    $input = htmlspecialchars($input);
                    return $input;
                }
                function validateUsername($username){
                    $username = sanitize_input($username);
                    if (empty($username) or strlen($username) < 3){
                        return FALSE;
                    }
                    return TRUE;
                }
                function validatePassword($password){
                    $password = sanitize_input($password);
                    if (empty($password) or
                        !preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$/", $password)){
                        return FALSE;
                    }
                    return TRUE;
                }
                function addnewuser($username, $password, $email, $phone){
                    global $mysqli;
                    $prepared_sql = "INSERT INTO users (username, password, email, phone) VALUES (?, password(?), ?, ?);";
                    echo "DEBUG:database.php ->addnewuser->prepared_sql= $prepared_sql\n";
                    if (!$stmt = $mysqli->prepare($prepared_sql)){
                        echo "prepare error!\n";
                        echo "$mysqli->error\n";
                        return FALSE;
                    }
                    $stmt->bind_param("ssss", $username, $password, $email, $phone);
                    if (!$stmt->execute()){
                        echo "execute error!\n";
                        echo "$mysqli->error\n";
                        return FALSE;
                    }
                    return TRUE;
                }
                //function to display posts on the page
                function getPosts($mysqli){
                    global $mysqli;
                    $sql = "SELECT * FROM posts";
                    $result = $mysqli->query($sql);
                    /*
                    * displaying the posts
                    * and giving the reply functionality to posts that arent from a specific user
                    */
                    while ($row = $result->fetch_assoc()){
                        $username = $row['username'];
                        $sql2 = "SELECT * FROM users WHERE username='$username'";
                        $result2 = $mysqli->query($sql2);
                        if ($row2 = $result2->fetch_assoc()){
                            echo "<div class='comment-box'><p>";
                            echo $row2['username']."<br>";
                            echo $row['date']."<br><br>";
                            echo nl2br($row['message']);
                            echo "</p>";

                            if ($_SESSION['username'] == $row2['username'] or $row2['enabled'] == 1){
                            echo "<form class='delete-form' method='POST' action='".deleteComments($mysqli)."'>
                                <input type='hidden' name='PostId' value='".$row['PostId']."'>
                                <button type='submit' name='postDelete'>Delete</button>
                            </form>
                            <form class='edit-form' method='POST' action='editPost.php'>
                                <input type='hidden' name='PostId' value='".$row['PostId']."'>
                                <input type='hidden' name='username' value='".$row['username']."'>
                                <input type='hidden' name='date' value='".$row['date']."'>
                                <input type='hidden' name='message' value='".$row['message']."'>
                                <button>Edit</button>
                            </form>";

                            }
                        }
                    }
                }//end getPosts()

                function addPost($newtitle, $newcontent, $newdate){
                    /*
                    global $mysqli;
                    $prepared_sql = "INSERT INTO users (username, password, email, phone) VALUES (?, password(?), ?, ?);";
                    echo "DEBUG:database.php ->addnewuser->prepared_sql= $prepared_sql\n";
                    if (!$stmt = $mysqli->prepare($prepared_sql)){
                        echo "prepare error!\n";
                        echo "$mysqli->error\n";
                        return FALSE;
                    }
                    $stmt->bind_param("ssss", $username, $password, $email, $phone);
                    if (!$stmt->execute()){
                        echo "execute error!\n";
                        echo "$mysqli->error\n";
                        return FALSE;
                    }
                    return TRUE;*/
                    global $mysqli;
                    if(isset($_POST['postSubmit'])){
                    $PostId = $_POST['PostId'];
                    $username = $_POST['username'];
                    $date = $_POST['date'];
                    $message = $_POST['message'];
                    /* values in the posts table
                    PostId int(20) NOT NULL AUTO_INCREMENT,
                    owner varchar(50) NOT NULL,
                    date datetime NOT NULL,
                    content text DEFAULT NULL,
                    title varchar(50) DEFAULT NULL*/

                    $prepared_sql = "INSERT INTO posts (owner, date, content, title) VALUES (?, ?, ?, ?);";
                    //create sql statement
                    echo "DEBUG:database.php ->addpost->prepared_sql= $prepared_sql\n";
                    if (!$stmt = $mysqli->prepare($prepared_sql)){
                        echo "prepare error!\n";
                        echo "$mysqli->error\n";
                        return FALSE;
                    }
                    $stmt->bind_param("ssss", $_SESSION["username"], $password, $email, $phone);
                    if (!$stmt->execute()){
                        echo "execute error!\n";
                        echo "$mysqli->error\n";
                        return FALSE;
                    }
                    return TRUE;
                    //$sql = "UPDATE posts SET message='$message' WHERE PostId='$PostId'";

                    $result = $mysqli->query($sql);
                    header("Location: index.php");
                    }
                }//end addPosts()

                function editPost($newtitle, $newcontent, $newdate, $postid){
                    global $mysqli;

            /*		if(isset($_POST['postSubmit'])){
                    $PostId = $_POST['PostId'];
                    $username = $_POST['username'];
                    $date = $_POST['date'];
                    $message = $_POST['message'];
            */

                    //create sql statement
                    $sql = "UPDATE posts SET message='$message' WHERE PostId='$PostId'";

                    $result = $mysqli->query($sql);
                    header("Location: index.php");

                }//end editPosts()

                function deletePost($mysqli){
                    global $mysqli;
                    if(isset($_POST['postDelete'])){
                    $PostId = $_POST['PostId'];

                    //create sql statement
                    $sql = "DELETE FROM posts WHERE PostId='$PostId'";

                    $result = $mysqli->query($sql);
                    header("Location: index.php");
                    }
                }//end deletePost()

                function setPost($mysqli){
                    global $mysqli;
                    if(isset($_POST['postSubmit'])){
                    $username = $_POST['username'];
                    $date = $_POST['date'];
                    $message = $_POST['message'];

                    //create sql statement
                        $sql = "INSERT INTO posts (username, date, message) VALUES ('$username', '$date', '$message')";

                    $result = $mysqli->query($sql);
                    }
                }//end setPost()

                function seeUsers($username){
                    global $mysqli;
                    $conn = new mysqli('localhost', 'admin', 'hermes', 'SecadTeam11');
                    $prepared_sql = "SELECT * FROM users WHERE username=? AND super = ?";
                    if(!$stmt = $mysqli->prepare($prepared_sql))
                        echo "Prepared Statement Error";
                    $sup = 1;
                    $stmt->bind_param("ss",$username,$sup);
                    if(!$stmt->execute()) echo "Execute Error in checking superuser";
                    if(!$stmt->store_result()) echo "Store result error in checking superuser";
                    $result = $stmt;
                    if($result->num_rows ==1){

                        $sql = "SELECT * FROM users";
                        $result = $conn->query($sql);
                        if ($result->num_rows > 0){
                        //output the data
                            echo"<h2>Superusers can view all registered users </h2>";
                            while ($row = $result->fetch_assoc()){
                                echo "<div><p>";
                                echo "Username: ";
                                echo $row["username"];
                                echo "     Password: Hidden     Email: ";
                                echo $row["email"];
                                echo "     Phone Number: ";
                                echo $row["phone"];
                                echo "     Super User: ";
                                echo $row["super"];
                                echo"</p></div><br>";
                            }
                        }
                        $conn->close();
                        return TRUE;
                    }
                    $conn->close();
                    return FALSE;
                }
            ?>
            ```
        * database.sql
            ```sql
            DROP TABLE IF EXISTS `comments`;
            DROP TABLE IF EXISTS `posts`;
            DROP TABLE IF EXISTS `users`;

            CREATE TABLE users (
            username varchar(50) PRIMARY KEY,
            password varchar(100) NOT NULL,
            email varchar(100) NOT NULL,
            phone varchar(100) NOT NULL,
            super tinyint(1) DEFAULT '0'
            );

            LOCK TABLES `users` WRITE;

            INSERT INTO `users` VALUES ('admin', password('hermes'), 'admin@secad.org', '1234567890',1),('beth', password('hosek'), 'hoseke1@udayton.edu','6302453869',1),('shara',password('shrestha'),'shresthas6@udayton.edu','3014379407',1);

            UNLOCK TABLES;

            CREATE TABLE posts (
            PostId int(20) NOT NULL AUTO_INCREMENT,
            owner varchar(50) NOT NULL,
            date datetime NOT NULL,
            content text DEFAULT NULL,
            title varchar(50) DEFAULT NULL,
            PRIMARY KEY (PostId),
            FOREIGN KEY (owner) REFERENCES `users`(`username`) ON DELETE CASCADE
            );

            CREATE TABLE comments (
                id int(11) AUTO_INCREMENT,
                PostId int(20) NOT NULL,
                content text NOT NULL,
                date timestamp DEFAULT CURRENT_TIMESTAMP, 
                owner varchar(50) NOT NULL,
                PRIMARY KEY (id),
                FOREIGN KEY (owner) REFERENCES `users`(`username`) ON DELETE CASCADE,
                FOREIGN KEY (postid) REFERENCES `posts`(`PostId`) ON DELETE CASCADE
            );
            ```
        * deletepost.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
                require "session_auth.php";
                require 'database.php';
                $postid = $_REQUEST["postid"];
                $username = $_REQUEST["username"];
                $nocsrftoken = $POST["nocsrftoken"];
                if (isset($postid) && isset($username)){
                    echo "DEBUG:deletepost.php->Got: postid=$postid; username=$username\n<br>";
                } else {
                    echo "no postid to delete";
                    exit();
                }

                if (!isset($nocsrftoken) or ($nocsrftoken!=$_SESSION['nocsrftoken'])){
                    echo "<script>alert('Cross-site request forgery is detected!');</script>";
                    header("Refresh:0; url=logout.php");
                    die();
                } else {
                    if ($username!=$_SESSION["username"]) {
                        echo "<script>alert('Cannot delete others posts!');</script>";
                        header("Refresh:0; url=logout.php");
                        die();
                    } else {
                    if (!"DELETE FROM posts WHERE postid=$postid;"->execute())
                        echo "Delete error.";
                }
            ?>
            ```
            ```html
            <a href="index.php">Home</a> | <a href="logout.php">Logout</a>
            ```
        * editcomment.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

                date_default_timezone_set('America/Los_Angeles');
                require ('database.php');
            ?>
            ```
            ```html
            <!DOCTYPE html>
            <html>
            <head>
                <link rel="stylesheet" type="text/css" href="style.css">
            <meta charset="utf-8">
                <title>Mercury Homepage</title>
            </head>
            <body>
            ```
            ```php 
            <?php
                $PostId = $_POST['PostId'];
                $username = $_POST['username'];
                $date = $_POST['date'];
                $message = $_POST['message'];

                echo "<form method='POST' action='".editComments($mysqli)."'>
                    <input type='hidden' name='PostId' value='".$PostId."'>
                    <input type='hidden' name='username' value='".$username."'>
                    <input type='hidden' name='date' value='".$date."'>
                    <textarea name='message'>".$message."</textarea><br>
                    <button type='submit' name='postSubmit'>Edit</button>
                </form>";
            ?>
            ```
            ``` html
            </body>
            </html>
            ```
        * editpost.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);

                date_default_timezone_set('America/New_York');
                require "database.php";
            ?>
            ```
            ```html
            <!DOCTYPE html>
            <html>
            <head>
                <link rel="stylesheet" type="text/css" href="style.css">
            <meta charset="utf-8">
                <title>Mercury Homepage</title>
            </head>
            <body>
            ```
            ```php 
            <?php
                $PostId = $_POST['PostId'];
                $username = $_POST['username'];
                $date = $_POST['date'];
                $content = $_POST['content'];

                echo "<form method='POST' action='".editPost($mysqli)."'>
                    <input type='hidden' name='PostId' value='".$PostId."'>
                    <input type='hidden' name='username' value='".$username."'>
                    <input type='hidden' name='date' value='".$date."'>
                    <textarea name='content'>".$content."</textarea><br>
                    <button type='submit' name='postSubmit'>Edit</button>
                    </form>";
            ?>
            ```
            ```html
            </body>
            </html>
            ```
        * form.php
            ```html
            <!DOCTYPE html>
            <html lang="en">
            <head>
            <link rel="stylesheet" type="text/css" href="style.css">
            <meta charset="utf-8">
            <title>Login page - Mercury</title>
            </head>
            <center>
            <body>
                    <div class="text-center" id = 'logo'>
                        <img src="./images/logos/Mercury1.png" alt="Mercury1">
                    </div>

                    <h1 style = "font-family:helvetica">SecAD Team 11</h1>
                    <h2 style = "font-family:helvetica">Beth Hosek and Shara Shrestha</h2>
            ```
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            //some code here
            echo "Current time: " . date("Y-m-d h:i:sa")
            ?>
            .
            ```
            ```html
                    <br>
                    <form action="index.php" method="POST" class="form login">
                            Username:<input type="text" class="text_field" name="username" /> <br>
                            Password: <input type="password" class="text_field" name="password" /> <br>
                            <button class="button" type="submit">
                            Login
                            </button>
                    </form>
                    <form action="registrationform.php" method="POST" class="form new user">
                            <button class="button" type="submit">
                            Sign Up
                            </button>
                    </form>
            </body>
            </center>
            </html>
            ```
        * index.php
            ```php
            <?php	
            require 'database.php';
            //require "session_auth.php";
            //DEBUG>echo "<script>alert('You got this far - 1');</script>";
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
            $lifetime = 15 * 60;
            $path = "/teamproject";
            $domain= "localhost";
            $secure = TRUE;
            $httponly = TRUE;
            session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
            session_start(); 

            $username;
            $password;
            
            //DEBUG>echo "<script>alert('You got this far - 2');</script>";
            if (isset($_POST["username"]) and isset($_POST["password"])){
                $username = $_POST["username"];
                $password = $_POST["password"];
                if (securechecklogin($username,$password)) {
                    $_SESSION["logged"] = TRUE;
                    $_SESSION["username"] = $_POST["username"];
                    $_SESSION["password"] = $_POST["password"];
                    $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
                }else{
                    echo "<script>alert('Invalid username/password');</script>";
                    unset($_SESSION["logged"]);
                    header("Refresh:0; url=form.php");
                    die();

                }
            }	
            //directly from phu's powerpoint
            if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE){
                echo "<script>alert('You have not logged in yet. Please login first!');</script>";
                header("Refresh:0; url=form.php");
                die();
            }
            //DEBUG>echo "<script>alert('You got this far - 4');</script>";
            if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
                echo "<script>alert('Session hijacking detected, please login again');</script>";
                header("Refresh:0; url=form.php");
                die();
            }

            /*if($username == NULL or $password == NULL)
            {
                $username = $_SESSION["username"];
                $password = $_SESSION["password"];
            }*/
            ?>
            ```
            ```html
            <!DOCTYPE html>
            <html>
            <head>
                <link rel="stylesheet" type="text/css" href="./style.css">
                <title> Mercury Homepage </title>
                
            </head>
            <script>
            function startTime() {
                document.getElementById('clock').innerHTML = new Date();
                setTimeout(startTime, 500);
            }
            var sanitizeHTML = function (str) {
            var temp = document.createElement('div');
            temp.textContent = str;
            return temp.innerHTML;
            };
            </script>
            <body>
            <center>
                <a href="changepasswordform.php">
                    <button class='homepage-button' type='submit' action='changepasswordform.php'>Change Password</button></a>
                <a href="logout.php">
                    <button class='homepage-button' type='submit' action='logout.php'>Log Out</button></a>
                <div id = 'logo'>
                        <img src="./images/logos/Mercury2.png" alt="Mercury2">
                </div>
                <h1 style = "font-family:helvetica">
                    Welcome, <?php echo htmlentities($_SESSION["username"]); ?>!
                </h1>

                <h2 style = "font-family:helvetica">Write a Post</h2>
                <!--- text area under here to write post, clicking the Post button should submit it via getPosts() function-->

                <form method="POST" action='index.php'>
                    <textarea class="title-bar" placeholder="Give your post a title" name="titlePost"></textarea><br>
                    <textarea placeholder="What's on your mind?" name="content"></textarea><br><br>
                    <button class='homepage-button' type='submit' name='Post'> Write Post</button>
                </form>

                <br><br><br>
            ```
            ```php
            <?php
                    $username = $_SESSION["username"];
                    $password = $_SESSION["password"];
                    $dbserver = "localhost";
                    $dbusername = "admin";
                    $dbpassword = "hermes";
                    $dbname = "SecadTeam11";
                    $date = date("Y-m-d H:i:s");

                    //connect to db for post echoing
                    $conn = new mysqli($dbserver, $dbusername, $dbpassword, $dbname);

                    $insertsql = "INSERT INTO posts (username, date, content, title) VALUES ('$username', '$date', 'some new content for testing', 'a new title')";
                        $conn->query($insertsql);

                    //query the database
                    $sql = "SELECT * FROM posts";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0){
                        //output the data
                        while ($row = $result->fetch_assoc()){
                            if ($row["username"] == $_SESSION["username"]){ //if the post is from the same user that's
                                echo "<div class='comment-box'><p><b>";									//logged in or if the user is a superuser
                                echo $row["username"]."</b><br>";
                                echo $row["title"];
                                echo " | ";
                                echo $row["date"]."<br>";
                                echo $row["content"];
                                echo "</p>";

                                //delete post button
                                echo "<form class='delete-form' method='POST' action='".deletePost($conn)."'>";
                                    echo "<input type='hidden' name='PostId' value='".$row['PostId']."'>";
                                    echo "<button type='submit' name='postDelete'>Delete</button>";
                                echo "</form>";

                                //edit post button
                                echo "<form class='edit-form' method='POST' action='editPost.php'>";
                                    echo "<input='hidden' name='PostId' value='".$row['PostId']."'";
                                    echo "<input='hidden' name='username' value='".$row['username']."'>";
                                    echo "<input='hidden' name='date' value='".$row['date']."'>";
                                    echo "<input='hidden' name='content' value='".$row['content']."'>";
                                    echo "<button>Edit</button>";
                                echo "</form>";

                                //end of content in comment box
                                echo "</div><br>";
                            } else {
                                echo "<div class='comment-box'><p><b>";
                            echo $row["username"]."</b><br>";
                            echo $row["title"];
                            echo " | ";
                            echo $row["date"]."<br>";
                            echo $row["content"];
                            echo "</p>";

                            //else you can reply to a post because it is not yours
                            echo "<form class='comment-form' method='POST' action='addcomment.php'>";
                                    echo "<input='hidden' name='PostId' value='".$row['PostId']."'";
                                    echo "<input='hidden' name='username' value='".$row['username']."'>";
                                    echo "<input='hidden' name='date' value='".$row['date']."'>";
                                    echo "<input='hidden' name='content' value='".$row['content']."'>";
                                    echo "<button>Comment</button>";
                                echo "</form>";
                            echo "</div><br>";
                            }
                        }
                    } else {
                        echo "0 results";
                    }
                    $conn->close();

                    seeUsers($_SESSION["username"]);
            ?>
            ```
            </center>
            </body>
            </html>
        * logout.php
            ```html
            <!DOCTYPE html>
            <html>
            <center>
            <head>
                <link rel="stylesheet" type="text/css" href="./style.css">
                <title> Mercury Logout Page </title>
            </head>
            <body>
                <div id = 'logo'>
                        <img src="./images/logos/Mercury1.png" alt="Mercury1">
                </div>
            </body>
            <script>
            ```
            ```php
            <?php
                session_start();
                session_destroy();
            ?>
            ```
            ```html
            </script>
            <body>
            <p style = "font-family:helvetica"> You are logged out! </p>

            <a href="form.php">
                    <button class='homepage-button' type='submit' action='form.php'>Login again</button></a>
                </body>
                </center>
                </html>
            ```
        * posts.php
            ```php
            <?php
            ini_set('display_errors', 1);
            ini_set('display_startup_errors', 1);
            error_reporting(E_ALL);
                require 'database.php';
                //template for retrieving posts from the database


                //using prepared sql statements
                $prepared_sql = "SELECT username, message, date, PostId FROM posts";

                //create prepared statement from the string
                if(!$stmt = $mysqli->prepare($prepared_sql))
                    echo "Prepared SQL Statement Error";

                //execute the statement
                if(!$stmt->execute())
                    echo "Execute error";
                $title = NULL; $content = NULL; $date=NULL; $PostId=NULL;
                if(!$stmt->bind_result($username, $message, $date, $PostId))
                    echo "Binding failed ";

                //Display the data from the variables (Should have HTML tags to provide a good presentation)
                while($stmt->fetch()){
                    echo htmlentities($username) . ", " . htmlentities($message) . ", " .
                    htmlentities($date) . "<br>";
                    echo '<div><form action="changepostform.php" method="POST" class="form login">
                                <input type="hidden" name="postid" value="<?php echo htmlentities(' . $PostId. '); ?>" /> <br>
                                <input type="hidden" name="username" value="<?php echo '. $_SESSION(["username"]) . '; ?>" />
                                <button name="edit-button" type="submit">
                                    Edit
                                </button>
                    </div></form>';
                }
            ?>
            ```
        * registrationform.php
            ```html
            <!DOCTYPE html>
            <html lang="en">
            <center>
            <head>
            <link rel="stylesheet" type="text/css" href="./style.css">
            <meta charset="utf-8">
            <title>Registration page - SecAD</title>
            <div id = 'logo'>
                        <img src="./images/logos/Mercury2.png" alt="Mercury2">
            </div>
            </head>
            <body>
            <link rel="stylesheet" type="text/css" href="./style.css">
                    <h1 style = "font-family:helvetica">Team Project, SecAD</h1>
                    <h2 style = "font-family:helvetica">Team 11: Beth Hosek and Shara Shrestha</h2>
            ```
            ```php
            <?php
            //some code here
            echo "Current time: " . date("Y-m-d h:i:sa")
            ?>
            ```
            ```html
                    <form action="addnewuser.php" method="POST" class="form login">
                            Username:<input type="text" class="text_field" name="username" pattern="^[\w]+$" title="Please enter a valid username" 
                            placeholder="Your username" 
                            onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');"/> <br>
                    
                            Password:<input type="password" class="text_field" name="password" required pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&])[\w!@#$%^&]{8,}$" 
                            placeholder="Your password"
                            title="Password must have at least 8 characters with 1 special symbol !@#$%^& 1 number, 1 lowercase, and 1 UPPERCASE" 
                            placeholder="Your password" 
                            onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: ''); form.repassword.pattern = this.value;"/> <br>
                            Retype Password: <input type="password" class="text_field" name="repassword"
                            placeholder="Retype your password" required
                            title="Password does not match"
                            onchange="this.setCustomValidity(this.validity.patternMismatch?this.title: '');"/> <br>

                            Email:<input type="text" class="text_field" name="email" required pattern="([a-z0-9][-a-z0-9_\+\.]*[a-z0-9])@([a-z0-9][-a-z0-9\.]*[a-z0-9]\.(edu|com|org)|([0-9]{1,3}\.{3}[0-9]{1,3}))"
                            title="Please enter a valid email" 
                            placeholder="Your email address" 
                            onchange="this.setCustomValidity(this.validity.patternMismatch ? this.title : '');"/> <br>
                            
                            Phone:<input type="text" class="text_field" name="phone"
                            required pattern="^[0-9]{10}$"
                            title="Please enter a valid 10 digit phone number without any characters, just numbers" 
                            placeholder="Your phone number" /> <br>

                            <button class="button" type="Register">
                            Sign Up
                            </button>
                    </form>
            </body>
            <center>
            </html>
            ```
        * session_auth.php
            ```php
            <?php
            $lifetime = 15 * 60; //15 min
            $path = "/teamproject";
            $domain= "localhost"; //personal IP address
            $secure = TRUE;
            $httponly = TRUE;
            session_set_cookie_params($lifetime, $path, $domain, $secure, $httponly);
            session_start();

            if ($_SESSION["logged"] != TRUE){
                //!isset($_SESSION["logged"]) or
                echo "<script>alert('You have not logged in yet. Please login first!');</script>";
                session_destroy();
                header("Refresh:0; url=form.php");
                die();
            }
            //DEBUG>echo "<script>alert('You got this far - 4');</script>";
            if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
                echo "<script>alert('Session hijacking detected, please login again');</script>";
                header("Refresh:0; url=form.php");
                die();
            }
            ?>
            ```
        * sessiontest.php
            ```php
            <?php
                session_start();

                if(isset($_SESSION['views']))
                    $_SESSION['views'] = $_SESSION['views']+ 1;
                else{
                    $_SESSION['views'] = 1;
                }
                echo "You have visited this page " . $_SESSION['views'] . " times"; 
            ?>
            ```
        * style.css
            ```css
                body {
                    background-color: #DDE9F1;                
                    }

                    textarea {
                        width: 700px;
                        height: 90px;
                        background-color: #fff;
                        resize: none;
                    }

                    button{
                        width: 100px;
                        height: 35px;
                        background-color: #bc1823;
                        border-radius: 8px;
                        border: 4px solid #EF3A5D;
                        color: #fff;
                        font-family: helvetica;
                        font-weight: 400;
                        cursor: pointer;
                        margin-bottom: 60px;
                    }

                    .comment-box {
                        width: 800px;
                        padding:20px;
                        margin-bottom: 4px;
                        background-color: #fff;
                        border-radius: 4px;
                        position: relative;
                        border: 4px solid #EF3A5D;
                    }

                    .comment-box p {
                        font-family: helvetica;
                        font-size: 16px;
                        line-height: 16px;
                        color: #282828;
                        font-weight: 120;
                    }
                    .edit-form {
                        position: absolute;
                        top: 0px;
                        right: 0px;
                    }

                    .edit-form button{
                        width: 50px;
                        height: 22px;
                        color: #bc1823;
                        background-color: #EF3A5D; 
                        opacity: 0.7;
                    }

                    .edit-form button:hover {
                        opacity: 1; 
                    }

                    .delete-form {
                        position: absolute;
                        top: 0px;
                        right: 70px;
                    }

                    .delete-form button{
                        width: 58px;
                        height: 22px;
                        color: #fff;
                        background-color: #EF3A5D; 
                        opacity: 0.7;
                    }

                    .delete-form button:hover {
                        opacity: 1; 
                    }
                    .homepage-button {
                        width: 270px;
                        height: 45px;
                        background-color: #bc1823;
                        border-radius: 8px;
                        border: 4px solid #EF3A5D;
                        color: #fff;
                        font-family: helvetica;
                        font-weight: 400;
                        cursor: pointer;
                        margin-bottom: 60px;
                        font-size: large;
                    }
                    .title-bar {
                        height: 40px;
                        width: 700px;
                        background-color: #fff;
                        resize: none;
                    }

                    .comment-form {
                        position: absolute;
                        top: 0px;
                        right: 50px;
                    }

                    .comment-form button {
                        width: 92px;
                        height: 22px;
                        color: #282828;
                        background-color: #fff;
                        opacity: 0.7;
                    }

                    .comment-form button:hover {
                        opacity: 1;
                    }
                    ```

![logo](./src/images/logos/Mercury1.png)
